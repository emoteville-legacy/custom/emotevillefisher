![](https://assets.emo.gg/logo/emoteville-gold3-small.png)
<br>
[Website](https://go.emo.gg/) - [Discord](https://go.emo.gg/discord) - [Issue Tracker](https://go.emo.gg/issue-tracker)

<br>
EmoteVilleFisher is a spigot plugin that allows the player to fish mobs from water and hook entities to do damage using energy, this is upgradable by creating different groups in the config.

The original documentation for this project is located [HERE](https://www.evernote.com/shard/s627/sh/a47bce34-0351-4417-a710-9fb2757abffb/f5e8a7f210cc78c543769ece36e0af3e).

It is possible to interact with other plugins using the commands and placeholders as an input/output system, this is how most of our plugins work.


**_Commands_**

| Command | Description |
| ------ | ------ |
| /fisher  | Enable/Disable your Fisher status. |
| /fisher (player) | Force a player to enable/disable their status. |

**_Permissions_**

| Command | Description |
| ------ | ------ |
| emoteville.fisher.(group) | Applies selected group from from config, if you enable/disable the feature then it re-checks the player group. |

**_Placeholders_**

| Placeholder | Description |
| ------ | ------ |
| %fisher_status% | Returns ON/OFF depending on if you have it toggled.|
| %fisher_energy% | Returns players current energy. |
| %fisher_energy_max% | Returns max amount of energy for player. |
| %fisher_regen% | Returns regen percent out of 100. |
| %fisher_regenbar_100% | Return 100 ":" grey which change to green to highlight your prevent progess to full regen. (https://i.imgur.com/UqT2wmC.png). |
| %fisher_regenbar_50% | Return 50 ":" grey which change to green to highlight your prevent progess to full regen.(https://i.imgur.com/UqT2wmC.png). |
| %fisher_regenbar_25% | Return 25 ":" grey which change to green to highlight your prevent progess to full regen.(https://i.imgur.com/UqT2wmC.png). |

