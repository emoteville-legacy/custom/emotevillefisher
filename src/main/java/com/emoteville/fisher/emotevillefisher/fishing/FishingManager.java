package com.emoteville.fisher.emotevillefisher.fishing;

import com.emoteville.fisher.emotevillefisher.EmoteVilleFisher;
import com.emoteville.fisher.emotevillefisher.group.Group;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.util.Location;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.util.Vector;

import java.util.*;

public class FishingManager implements Listener {

    private final EmoteVilleFisher plugin;

    private final List<FishingPlayer> players = new ArrayList<>();
    private final Map<FishHook, FishingPlayer> fishHookPlayerMap = new HashMap<>();

    public FishingManager(EmoteVilleFisher plugin) {
        this.plugin = plugin;

        Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () -> {
            players.forEach(fishingPlayer -> {
                Group group = fishingPlayer.getGroup();
                if (group != null && fishingPlayer.getEnergy() < group.getEnergyMax()) {
                    fishingPlayer.setEnergy(Math.min(fishingPlayer.getEnergy() + ((double) group.getEnergyMax() / (double) group.getRegen()) / 20, group.getEnergyMax()));
                    // TODO: Remove this, it is for testing only
                    //fishingPlayer.getPlayer().sendMessage(ChatColor.GREEN + "REGEN: " + fishingPlayer.getRegenPercent());
                }
            });
        }, 0, 1);

        Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            List<FishHook> removeList = new ArrayList<>();
            fishHookPlayerMap.forEach((fishHook, fishingPlayer) -> {
                Group group = fishingPlayer.getGroup();
                Player player = fishingPlayer.getPlayer();

                if (!fishingPlayer.getStatus() || group == null || fishHook.isDead()) {
                    removeList.add(fishHook);
                } else if (fishHook.getState() == FishHook.HookState.HOOKED_ENTITY
                        && fishHook.getHookedEntity() != null
                        && fishHook.getHookedEntity() instanceof LivingEntity) {
                    LivingEntity hookedEntity = (LivingEntity) fishHook.getHookedEntity();

                    if (fishingPlayer.getEnergy() >= group.getHookCost()) {
                        hookedEntity.damage(fishingPlayer.getGroup().getHookDamage());
                        fishingPlayer.setEnergy(fishingPlayer.getEnergy() - group.getHookCost());
                        fishingPlayer.setNotified(false);
                        hookedEntity.getWorld().spawnParticle(group.getParticle(), hookedEntity.getLocation(), group.getParticleAmount() / 2);
                    } else {
                        if (!fishingPlayer.hasBeenNotified()) {
                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfigManager().getMessage("notEnoughEnergy")));
                            fishingPlayer.setNotified(true);
                        }
                    }
                }
            });
            removeList.forEach(fishHookPlayerMap::remove);
        }, 0, 5);

        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onPlayerFish(PlayerFishEvent event) {
        Player player = event.getPlayer();
        FishingPlayer fishingPlayer = getPlayer(player);
        Group group = fishingPlayer.getGroup();
        World world = player.getWorld();
        PlayerFishEvent.State state = event.getState();

        if (!fishingPlayer.getStatus() || group == null) return;


        Location loc = BukkitAdapter.adapt(player.getLocation());
        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        RegionQuery query = container.createQuery();
        ApplicableRegionSet set = query.getApplicableRegions(loc);

        LocalPlayer localPlayer = WorldGuardPlugin.inst().wrapPlayer(player);
        if (!set.testState(localPlayer, plugin.getFishingFlag())) {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfigManager().getMessage("worldGuardDeny")));
            event.setCancelled(true);
            return;
        }

        switch (state) {
            case FISHING: {
                // TODO: Remove this, it is for testing only
                //event.getHook().setMinWaitTime(0);
                //event.getHook().setMaxWaitTime(5);

                fishHookPlayerMap.put(event.getHook(), fishingPlayer);
                break;
            }
            case BITE: {
                if (fishingPlayer.getEnergy() < group.getCost()) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfigManager().getMessage("notEnoughEnergy")));
                    event.setCancelled(true);
                } else {
                    player.sendTitle("", ChatColor.translateAlternateColorCodes('&', plugin.getConfigManager().getMessage("caughtMob")), 10, 70, 20);
                }
                break;
            }
            case CAUGHT_FISH: {
                Item item = (Item) event.getCaught();
                Vector velocity = item.getVelocity().multiply(2);
                item.remove();

                List<Class<? extends Entity>> entities = group.getEntities();
                Class<? extends Entity> entityClass = entities.get(new Random().nextInt(entities.size()));
                Entity entity = world.spawn(event.getHook().getLocation(), entityClass);

                world.spawnParticle(group.getParticle(), entity.getLocation(), group.getParticleAmount());

                Bukkit.getScheduler().runTaskLater(plugin, () -> entity.setVelocity(velocity), 1);

                fishingPlayer.setEnergy(fishingPlayer.getEnergy() - group.getCost());
                break;
            }
        }
    }

    public FishingPlayer getPlayer(Player player) {
        for (FishingPlayer fishingPlayer : players) {
            if (fishingPlayer.getPlayer().equals(player)) {
                return fishingPlayer;
            }
        }
        FishingPlayer fishingPlayer = new FishingPlayer(player, plugin);
        players.add(fishingPlayer);
        return fishingPlayer;
    }
}
