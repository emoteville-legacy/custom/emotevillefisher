package com.emoteville.fisher.emotevillefisher.fishing;

import com.emoteville.fisher.emotevillefisher.EmoteVilleFisher;
import com.emoteville.fisher.emotevillefisher.group.Group;
import org.bukkit.entity.Player;

public class FishingPlayer {

    private final EmoteVilleFisher plugin;

    private final Player player;

    private boolean status = true;
    private boolean notified;
    private Group group;
    private double energy;

    public FishingPlayer(Player player, EmoteVilleFisher plugin) {
        this.player = player;
        this.plugin = plugin;

        group = plugin.getGroupManager().getGroup(player);
        energy = group.getEnergyMax();
    }

    public Player getPlayer() {
        return player;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean hasBeenNotified() {
        return notified;
    }

    public void setNotified(boolean notified) {
        this.notified = notified;
    }

    public Group getGroup() {
        group = plugin.getGroupManager().getGroup(player);
        return group;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public double getRegenPercent() {
        return energy / (double) group.getEnergyMax() * 100;
    }
}
