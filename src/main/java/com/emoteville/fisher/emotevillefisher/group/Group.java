package com.emoteville.fisher.emotevillefisher.group;

import org.bukkit.Particle;
import org.bukkit.entity.Entity;

import java.util.List;

public class Group {

    private final String name;
    private final List<Class<? extends Entity>> entities;
    private final int energyMax, cost, hookCost, regen, particleAmount;
    private final double hookDamage;
    private final Particle particle;

    public Group(String name, List<Class<? extends Entity>> entities, int energyMax, int cost, double hookDamage, int hookCost, int regen, Particle particle, int particleAmount) {
        this.name = name;
        this.entities = entities;
        this.energyMax = energyMax;
        this.cost = cost;
        this.hookDamage = hookDamage;
        this.hookCost = hookCost;
        this.regen = regen;
        this.particle = particle;
        this.particleAmount = particleAmount;
    }

    public String getName() {
        return name;
    }

    public List<Class<? extends Entity>> getEntities() {
        return entities;
    }

    public int getEnergyMax() {
        return energyMax;
    }

    public int getCost() {
        return cost;
    }

    public int getHookCost() {
        return hookCost;
    }

    public double getHookDamage() {
        return hookDamage;
    }

    public int getRegen() {
        return regen;
    }

    public Particle getParticle() {
        return particle;
    }

    public int getParticleAmount() {
        return particleAmount;
    }
}
