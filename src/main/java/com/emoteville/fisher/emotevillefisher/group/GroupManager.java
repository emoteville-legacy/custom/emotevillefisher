package com.emoteville.fisher.emotevillefisher.group;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class GroupManager {

    private final List<Group> groups = new ArrayList<>();

    public void addGroup(Group group) {
        groups.add(group);
    }

    public Group getGroup(Player player) {
        Group group = null;
        for (Group g : groups) {
            if (player.hasPermission("emoteville.fisher." + g.getName())) {
                group = g;
            }
        }
        return group;
    }
}
