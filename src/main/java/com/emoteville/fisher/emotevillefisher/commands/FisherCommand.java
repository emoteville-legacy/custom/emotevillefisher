package com.emoteville.fisher.emotevillefisher.commands;

import com.emoteville.fisher.emotevillefisher.EmoteVilleFisher;
import com.emoteville.fisher.emotevillefisher.fishing.FishingPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class FisherCommand implements CommandExecutor {

    private final EmoteVilleFisher plugin;

    public FisherCommand(EmoteVilleFisher plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        Player player = null;

        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You must specify a player");
            } else {
                player = (Player) sender;
            }
        } else {
            player = Bukkit.getPlayer(args[0]);
        }

        if (player == null) {
            sender.sendMessage(ChatColor.RED + "Could not find player");
        } else {
            FishingPlayer fishingPlayer = plugin.getFishingManager().getPlayer(player);
            fishingPlayer.setStatus(!fishingPlayer.getStatus());
            if (fishingPlayer.getStatus()) {
                fishingPlayer.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfigManager().getMessage("statusOn")));
            } else {
                fishingPlayer.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfigManager().getMessage("statusOff")));
            }
        }
        return true;
    }
}
