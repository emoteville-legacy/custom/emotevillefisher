package com.emoteville.fisher.emotevillefisher.papi;

import com.emoteville.fisher.emotevillefisher.EmoteVilleFisher;
import com.emoteville.fisher.emotevillefisher.fishing.FishingPlayer;
import com.emoteville.fisher.emotevillefisher.group.Group;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;

public class FisherExpansion extends PlaceholderExpansion {

    private final EmoteVilleFisher plugin;

    public FisherExpansion(EmoteVilleFisher plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public @NotNull String getAuthor() {
        return "VendoAU";
    }

    @Override
    public @NotNull String getIdentifier() {
        return "fisher";
    }

    @Override
    public @NotNull String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onRequest(OfflinePlayer offlinePlayer, @NotNull String identifier) {
        if (offlinePlayer.isOnline()) {
            Player player = offlinePlayer.getPlayer();
            FishingPlayer fishingPlayer = plugin.getFishingManager().getPlayer(player);

            if (fishingPlayer != null) {
                Group group = fishingPlayer.getGroup();
                if (identifier.equals("status")) {
                    return fishingPlayer.getStatus() + "";
                } else if (group != null) {
                    switch (identifier) {
                        case "energy":
                            return fishingPlayer.getEnergy() + "";
                        case "group":
                            return group.getName();
                        case "energy_max":
                            return group.getEnergyMax() + "";
                        case "hook_damage":
                            return group.getHookDamage() + "";
                        case "regen":
                            return fishingPlayer.getRegenPercent() + "";
                    }
                }
                if (identifier.startsWith("regenbar_")) {
                    int amount;

                    try {
                        amount = (int) Math.min(25 * (Math.round((double) Integer.parseInt(identifier.substring(9)) / 25)), 100);
                    } catch (NumberFormatException e) {
                        amount = 100;
                    }
                    return getRegenBar(fishingPlayer, amount);
                }
            }
        }
        return null;
    }

    private String getRegenBar(FishingPlayer fishingPlayer, int amount) {
        int energy = (int) (fishingPlayer.getRegenPercent() / 100 * amount);
        int depleted = amount - energy;

        return ChatColor.GREEN + String.join("", Collections.nCopies(energy, "|"))
                + ChatColor.GRAY + String.join("", Collections.nCopies(depleted, "|"));
    }
}
