package com.emoteville.fisher.emotevillefisher;

import com.emoteville.fisher.emotevillefisher.commands.FisherCommand;
import com.emoteville.fisher.emotevillefisher.config.ConfigManager;
import com.emoteville.fisher.emotevillefisher.fishing.FishingManager;
import com.emoteville.fisher.emotevillefisher.group.GroupManager;
import com.emoteville.fisher.emotevillefisher.papi.FisherExpansion;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import org.bukkit.plugin.java.JavaPlugin;

public final class EmoteVilleFisher extends JavaPlugin {

    private StateFlag fishingFlag;

    private GroupManager groupManager;
    private ConfigManager configManager;
    private FishingManager fishingManager;

    @Override
    public void onLoad() {
        FlagRegistry registry = WorldGuard.getInstance().getFlagRegistry();
        try {
            StateFlag flag = new StateFlag("fishing", true);
            registry.register(flag);
            fishingFlag = flag;
        } catch (FlagConflictException e) {
            Flag<?> existing = registry.get("fishing");
            if (existing instanceof StateFlag) {
                fishingFlag = (StateFlag) existing;
            } else {
                getLogger().severe("There is already a flag named fishing");
            }
        }
    }

    @Override
    public void onEnable() {
        groupManager = new GroupManager();
        configManager = new ConfigManager(this);
        fishingManager = new FishingManager(this);

        getCommand("fisher").setExecutor(new FisherCommand(this));

        if (getServer().getPluginManager().getPlugin("PlaceholderAPI") != null) {
            new FisherExpansion(this).register();
        }
    }

    public StateFlag getFishingFlag() {
        return fishingFlag;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public GroupManager getGroupManager() {
        return groupManager;
    }

    public FishingManager getFishingManager() {
        return fishingManager;
    }
}
