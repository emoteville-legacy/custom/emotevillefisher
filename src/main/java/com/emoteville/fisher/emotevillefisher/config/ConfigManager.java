package com.emoteville.fisher.emotevillefisher.config;

import com.emoteville.fisher.emotevillefisher.EmoteVilleFisher;
import com.emoteville.fisher.emotevillefisher.group.Group;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigManager {

    private final EmoteVilleFisher plugin;

    private Map<String, Object> groupsMap;
    private Map<String, String> messagesMap;

    public ConfigManager(EmoteVilleFisher plugin) {
        this.plugin = plugin;

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Map<String, Map<String, Object>> configMap = new HashMap<>();
        File configFile = new File(plugin.getDataFolder(), "config.json");

        try {
            if (!configFile.exists()) {
                plugin.saveResource(configFile.getName(), false);
            }
            configMap = gson.fromJson(new FileReader(configFile), HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        groupsMap = configMap.get("groups");

        messagesMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : configMap.get("messages").entrySet()) {
            if (entry.getValue() instanceof String) {
                messagesMap.put(entry.getKey(), (String) entry.getValue());
            }
        }

        getGroups();
    }

    private void getGroups() {
        for (Map.Entry<String, Object> entry : groupsMap.entrySet()) {
            String groupName = entry.getKey();
            Map<String, Object> groupMap = (Map<String, Object>) entry.getValue();

            List<Class<? extends Entity>> entities = new ArrayList<>();
            for (String mob : (List<String>) groupMap.get("mobs")) {
                entities.add(EntityType.valueOf(mob).getEntityClass());
            }
            int energy = ((Number) groupMap.get("energy")).intValue();
            int cost = ((Number) groupMap.get("cost")).intValue();
            int hookDamage = ((Number) groupMap.get("hook-dmg")).intValue();
            int hookCost = ((Number) groupMap.get("hook-cost")).intValue();
            int regen = ((Number) groupMap.get("regen")).intValue();
            Particle particle = Particle.valueOf((String) groupMap.get("particle"));
            int particleAmount = ((Number) groupMap.get("particleAmount")).intValue();

            plugin.getGroupManager().addGroup(new Group(groupName, entities, energy, cost, hookDamage, hookCost, regen, particle, particleAmount));
        }
    }

    public String getMessage(String message) {
        return messagesMap.get(message);
    }
}
